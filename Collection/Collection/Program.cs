﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collection
{
    class SuperHero
    {
        private String name { get; set; }
        private String power { get; set; }
        private String brand { get; set; }
        private int age { get; set; }



        static void Main(string[] args)
        {
            //create a collection
            var ListOfSuperHeroes = new List<SuperHero>();
            var ListOfSuperHeroes2 = new List<SuperHero>();
            ListOfSuperHeroes = GetFiveSuperHeroes();
            
            var temphero1 = new SuperHero();
            temphero1.age =40;
            temphero1.name = "spiderman";
            temphero1.power = "web";
            var temphero2 = new SuperHero();
            temphero1.age = 30;
            temphero1.name = "batman";
            temphero1.power = "darknight";
            ListOfSuperHeroes2.Add(temphero1);
            ListOfSuperHeroes2.Add(temphero2);

            var finallist = new List<SuperHero>();
            finallist.AddRange(ListOfSuperHeroes);
            finallist.AddRange(ListOfSuperHeroes2);
            //ShowSuperHeroes(finallist);
            //ShowSuperHeroes(ListOfSuperHeroes);
            //ShowSuperHeroes(ListOfSuperHeroes2);
            //int age = 10;
            // var temphero = listOfSuperHeroes.Select(x => x).Where(x => x.age == 10);
            //  SuperHero returnedHero = findByAge(age,finallist);
            string name = "batman";
            SuperHero returnedname= findByName(name, finallist);
           
            Console.ReadLine();
        }

        private static SuperHero findByName(string name, List<SuperHero> finallist)
        {
            SuperHero tempheroname = new SuperHero();
            
            tempheroname = finallist.Select(x => x).Where(x => x.name == name).FirstOrDefault();
            return tempheroname;
        }

        /*private static SuperHero findByAge(int age, List<SuperHero> finallist)
            {
                SuperHero temphero = new SuperHero();
                //var temphero = listOfSuperHeroes.Select(x => x).Where(x => x.age == 10);
                temphero = finallist.Select(x => x).Where(x => x.age == age).FirstOrDefault();
                return temphero;
            }
        */

        private static void ShowSuperHeroes(List<SuperHero> listOfSuperHeroes)
        {
            //.OrderByDescending(x => x.Delivery.SubmissionDate);
            // listOfSuperHeroes.OrderByDescending(x=>x.age);
            
   
            listOfSuperHeroes = listOfSuperHeroes.OrderByDescending(x => x.age).ToList();
            foreach (var x in listOfSuperHeroes)
            {
                Console.WriteLine("------------------");
                Console.WriteLine("Name " + x.name);
                Console.WriteLine("Age " + x.age);
                Console.WriteLine("Power  " + x.power);
                Console.WriteLine("Brand " + x.brand);
                Console.WriteLine("------------------");
            }
        }
        private static List<SuperHero> GetFiveSuperHeroes()
        {
            //create a n empty list.
            var tempList = new List<SuperHero>();

            //five super heroes. 
            var name = "";
            var power = "";
            var brand = "";
            var age = 10;

            //five super heroes. 
            var tempHero = new SuperHero();
            var tempHero2 = new SuperHero();
            var tempHero3 = new SuperHero();
            var tempHero4 = new SuperHero();
            var tempHero5 = new SuperHero();
            var tempHero6 = new SuperHero();

            name = "A";
            power = "Money";
            brand = "DC";
            age = 55;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);


            name = "antman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero2.name = name;
            tempHero2.power = power;
            tempHero2.brand = brand;
            tempHero2.age = age;

            //add this hero to list. 
            tempList.Add(tempHero2);

            name = "hitman";
            power = "Money";
            brand = "DC";
            age = 29;

            tempHero3.name = name;
            tempHero3.power = power;
            tempHero3.brand = brand;
            tempHero3.age = age;

            //add this hero to list. 
            tempList.Add(tempHero3);

            name = "B";
            power = "Money";
            brand = "DC";
            age = 78;

            tempHero4.name = name;
            tempHero4.power = power;
            tempHero4.brand = brand;
            tempHero4.age = age;

            //add this hero to list. 
            tempList.Add(tempHero4);

            name = "Wonderwoman";
            power = "Money";
            brand = "DC";
            age = 100;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);

            name = "c";
            power = "Money";
            brand = "DC";
            age = 96;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);


            return tempList;
        }
    }
}

