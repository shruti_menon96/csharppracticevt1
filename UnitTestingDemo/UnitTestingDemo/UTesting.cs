﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestingDemo
{
    class UTesting
    {
        ////unit test for code reuse.
        //copy paste this code file to build new unit tests quickly.
        [TestClass]
        public class SampleTest
        {
            [TestMethod]
            public void TestSampleOne()
            {
                var actual = true;
                var expected = true;
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSampleTwo()
             {
                var actual = true;
                var expected = false;
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSampleThree()
            {
                var inp1 = 13237.85;
                var inp2 = 17897.65;

                var actual = 31135.5;
                var expected = Program.sumOfTwoNumbers(inp1,inp2);
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSampleFour()
            {
                var inp1 = 13237.85;
                var inp2 = 17897.65;

                var actual = Program.sumOfTwoNumbers(inp1, inp2);
                var expected = 31135.5;
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSampleFive()
            {
                var inp1 = 13237.85;
                var inp2 = 17897.65;

                var actual = Program.sumOfTwoNumbers(inp1, inp2);
                var expected = 31135.5;
                Assert.AreEqual(expected, actual);
            }


        }
    }
}
