﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestingDemo
{
    class Program
    {
       static void Main(string[] args)
        {
            double num1 = 10.0;
            double num2 = 20.0;
            double resultOfAddition = 0.0;
            resultOfAddition = sumOfTwoNumbers( num1, num2);
            Console.ReadLine();
        }

        public static double sumOfTwoNumbers(double num1, double num2)
        {
            double result = num1 + num2;
            return result;
        }

        
    }
}
