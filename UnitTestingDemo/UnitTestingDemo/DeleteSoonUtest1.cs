﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestingDemo
{
    ////unit test for code reuse.
    //copy paste this code file to build new unit tests quickly.
    [TestClass]
    public class SampleTest2
    {
        [TestMethod]
        public void TestcheckForChangeSolicitor2()
        {
            var actual = true;
            var expected = true;
            Assert.AreEqual(expected, actual);
        }
    }
}
