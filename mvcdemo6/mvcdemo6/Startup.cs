﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(mvcdemo6.Startup))]
namespace mvcdemo6
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
